﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

using Autofac;
using Autofac.Integration.Presentation;

namespace DocsVision.NormativeDocuments
{
	using Models;
	using ViewModels.Main;
	using Views.Main;

	internal class EntryPoint
	{
		[STAThread]
		internal static void Main(string[] args)
		{
			// register - resolve - release in action

			var builder = new ContainerBuilder();

			var mainWindow = new MainWindow();

			builder.RegisterType<MainApplication>();
			builder.RegisterNavigationService(mainWindow.NavigationService);
			builder.RegisterServices(RegisterApplicationServices);

			builder.Bind<RootViewModel, RootView>();

			using (IContainer container = builder.Build())
			{
				container.Resolve<MainApplication>().Run(mainWindow);
			}
		}
		
		private static void RegisterApplicationServices(ContainerBuilder builder)
		{
			builder.RegisterType<DocsVisionModel>().SingleInstance();
		}
	}
}