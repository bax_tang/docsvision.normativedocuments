﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

using Autofac;
using Autofac.Integration.Presentation;

namespace DocsVision.NormativeDocuments
{
	using ViewModels.Main;

	public partial class MainApplication : Application
	{
		private readonly INavigationService _navigationService;

		public MainApplication(INavigationService navigationService) : base()
		{
			InitializeComponent();

			_navigationService = navigationService;
		}

		protected override void OnNavigated(NavigationEventArgs args)
		{
			base.OnNavigated(args);
		}

		protected override void OnNavigating(NavigatingCancelEventArgs args)
		{
			base.OnNavigating(args);
		}

		protected override void OnStartup(StartupEventArgs args)
		{
			base.OnStartup(args);
			
			_navigationService.NavigateToViewModel<RootViewModel>();
		}
	}
}