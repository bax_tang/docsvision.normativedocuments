﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;

namespace DocsVision.NormativeDocuments
{
	public partial class MainWindow : NavigationWindow
	{
		public MainWindow() : base()
		{
			InitializeComponent();
		}
		
		private void CanCloseWindow(object sender, CanExecuteRoutedEventArgs args)
		{
			args.CanExecute = true;
		}

		private void CloseWindow(object sender, ExecutedRoutedEventArgs args)
		{
			SystemCommands.CloseWindow(this);
		}
	}
}