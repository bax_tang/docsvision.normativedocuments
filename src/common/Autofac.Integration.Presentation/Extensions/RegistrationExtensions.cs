﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

using Autofac.Builder;
using Autofac.Core;
using Autofac.Core.Lifetime;

namespace Autofac.Integration.Presentation
{
	public static class RegistrationExtensions
	{
		#region Public class methods

		public static void RegisterNavigationService(this ContainerBuilder builder, NavigationService navigationService)
		{
			builder.RegisterInstance(navigationService);
			builder.Register(CreateNavigationService)
				.As<INavigationService>()
				.SingleInstance();
		}

		public static void RegisterServices(this ContainerBuilder builder, Action<ContainerBuilder> registrationCallback)
		{
			registrationCallback(builder);
		}

		public static void Bind<TViewModel, TView>(this ContainerBuilder builder,
			InstanceLifetime viewLifetime = InstanceLifetime.Single,
			InstanceLifetime viewModelLifetime = InstanceLifetime.Single) where TViewModel : IViewModel where TView : IView<TViewModel>
		{
			var viewBuilder = builder.RegisterType<TView>()
				.As<IView, IView<TViewModel>>()
				.AsSelf();
			ApplyInstanceLifetime(viewBuilder, viewLifetime);

			var activator = new ViewModelActivator<TViewModel, TView>();

			var viewModelBuilder = builder.RegisterType<TViewModel>()
				.AsImplementedInterfaces()
				.AsSelf()
				.OnActivating(activator.ActivateViewModel);
			ApplyInstanceLifetime(viewModelBuilder, viewModelLifetime);
		}
		#endregion

		#region Private class methods

		private static INavigationService CreateNavigationService(IComponentContext context)
		{
			var lifetimeScope = context.Resolve<ILifetimeScope>();
			var navigationService = context.Resolve<NavigationService>();

			var service = new DefaultNavigationService(lifetimeScope, navigationService);
			return service;
		}

		private static void ApplyInstanceLifetime<TLimit, TActivatorData, TRegistrationStyle>(
			IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> builder,
			InstanceLifetime instanceLifetime)
		{
			if (builder == null)
			{
				throw new ArgumentNullException(nameof(builder), "Registration builder cannot be null.");
			}

			switch (instanceLifetime)
			{
				case InstanceLifetime.Single:
					builder.SingleInstance();
					break;
				case InstanceLifetime.Scoped:
					builder.InstancePerLifetimeScope();
					break;
				case InstanceLifetime.Transient:
					builder.InstancePerDependency();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(instanceLifetime), "Unknown lifetime type.");
			}
		}
		#endregion

		#region Nested types

		private class ViewModelActivator<TViewModel, TView> where TViewModel : IViewModel where TView : IView<TViewModel>
		{
			public void ActivateViewModel(IActivatingEventArgs<TViewModel> args)
			{
				IView attachedView = args.Context.Resolve<TView>();

				args.Instance.AttachView(attachedView);
			}
		}
		#endregion
	}
}