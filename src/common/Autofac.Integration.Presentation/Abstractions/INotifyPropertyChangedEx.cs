﻿using System;
using System.ComponentModel;

namespace Autofac.Integration.Presentation
{
	public interface INotifyPropertyChangedEx : INotifyPropertyChanged
	{
		bool IsNotifying { get; set; }
	}
}