﻿using System;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace Autofac.Integration.Presentation
{
	public interface IView
	{
		event RoutedEventHandler Loaded;

		event RoutedEventHandler Unloaded;

		object DataContext { get; set; }

		Dispatcher Dispatcher { get; }

		NavigationService NavigationService { get; }
	}

	public interface IView<TViewModel> : IView where TViewModel : IViewModel
	{
		TViewModel ViewModel { get; }
	}
}