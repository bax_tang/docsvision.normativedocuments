﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Autofac.Integration.Presentation
{
	public abstract class PropertyChangedBase : INotifyPropertyChangedEx, INotifyPropertyChanged
	{
		public virtual bool IsNotifying { get; set; } = true;

		public virtual event PropertyChangedEventHandler PropertyChanged;

		protected internal PropertyChangedBase() { }

		public virtual void NotifyOnPropertyChange([CallerMemberName] string propertyName = null)
		{
			if (IsNotifying && PropertyChanged != null)
			{
				OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
			}
		}

		protected void OnPropertyChanged(PropertyChangedEventArgs args)
		{
			PropertyChanged?.Invoke(this, args);
		}
	}
}