﻿using System;
using System.Windows.Navigation;

namespace Autofac.Integration.Presentation
{
	internal class DefaultNavigationService : INavigationService
	{
		#region Fields and properties

		private readonly ILifetimeScope _lifetimeScope;
		private readonly NavigationService _navigationService;
		#endregion

		#region Constructors

		internal DefaultNavigationService(ILifetimeScope lifetimeScope, NavigationService navigationService)
		{
			_lifetimeScope = lifetimeScope;
			_navigationService = navigationService;
		}
		#endregion

		#region INavigationService implementation

		public bool NavigateToViewModel<TViewModel>() where TViewModel : IViewModel
		{
			var viewModel = _lifetimeScope.Resolve<TViewModel>();

			return _navigationService.Navigate(viewModel.AttachedView);
		}

		public bool NavigateToViewModel<TViewModel, TParameter>(TParameter parameter) where TViewModel : IViewModel<TParameter>
		{
			var viewModel = _lifetimeScope.Resolve<TViewModel>();

			viewModel.Parameter = parameter;

			return _navigationService.Navigate(viewModel.AttachedView);
		}
		#endregion
	}
}