﻿using System;
using System.ComponentModel;

namespace Autofac.Integration.Presentation
{
	public abstract class ViewModelBase : ViewAware, IViewModel
	{
		protected internal ViewModelBase() : base() { }
	}
}