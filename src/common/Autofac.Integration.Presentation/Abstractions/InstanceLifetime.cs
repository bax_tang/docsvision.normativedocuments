﻿using System;

namespace Autofac
{
	public enum InstanceLifetime
	{
		Single,
		Scoped,
		Transient
	}
}