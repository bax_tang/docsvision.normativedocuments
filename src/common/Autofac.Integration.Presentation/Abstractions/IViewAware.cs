﻿using System;

namespace Autofac.Integration.Presentation
{
	public interface IViewAware
	{
		IView AttachedView { get; }

		void AttachView(IView view);
	}
}