﻿using System;

namespace Autofac.Integration.Presentation
{
	public interface INavigationService
	{
		bool NavigateToViewModel<TViewModel>() where TViewModel : IViewModel;

		bool NavigateToViewModel<TViewModel, TParameter>(TParameter parameter) where TViewModel : IViewModel<TParameter>;
	}
}