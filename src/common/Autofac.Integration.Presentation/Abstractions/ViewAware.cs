﻿using System;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace Autofac.Integration.Presentation
{
	public abstract class ViewAware : PropertyChangedBase, IViewAware
	{
		#region Fields and properties

		private IView _attachedView;

		public IView AttachedView => _attachedView;
		#endregion

		#region Constructors

		protected internal ViewAware() : base() { }
		#endregion

		#region IViewAware implementation

		public void AttachView(IView view)
		{
			if (view == null)
			{
				throw new ArgumentNullException(nameof(view), "Attached view cannot be null.");
			}

			_attachedView = view;

			view.DataContext = this;
			view.Loaded += AttachedViewLoaded;
			view.Unloaded += AttachedViewUnloaded;
		}
		#endregion

		#region Class methods

		protected virtual void OnActivated() { }

		protected virtual void OnDeactivated() { }

		private void AttachedViewLoaded(object sender, RoutedEventArgs args)
		{
			_attachedView.NavigationService.Navigating += OnNavigating;
			_attachedView.NavigationService.Navigated += OnNavigated;

			OnActivated();
		}

		private void AttachedViewUnloaded(object sender, RoutedEventArgs args)
		{
			_attachedView.NavigationService.Navigated -= OnNavigated;
			_attachedView.NavigationService.Navigating -= OnNavigating;

			OnDeactivated();
		}

		private void OnNavigating(object sender, NavigatingCancelEventArgs args)
		{

		}

		private void OnNavigated(object sender, NavigationEventArgs args)
		{

		}

		protected void ExecuteOnUIThread(Action callback)
		{
			_attachedView.Dispatcher.Invoke(callback, DispatcherPriority.Normal);
		}
		#endregion
	}
}