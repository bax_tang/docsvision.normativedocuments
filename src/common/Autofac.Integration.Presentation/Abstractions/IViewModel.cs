﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Autofac.Integration.Presentation
{
	public interface IViewModel : IViewAware, INotifyPropertyChangedEx { }

	public interface IViewModel<TParameter> : IViewModel
	{
		TParameter Parameter { set; }
	}
}