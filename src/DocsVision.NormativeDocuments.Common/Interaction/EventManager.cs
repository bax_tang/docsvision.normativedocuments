﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DocsVision.NormativeDocuments.Common
{
	public static class EventManager
	{
		public static IAsyncResult RaiseAsync(Action handler)
		{
			return EventWrapper.CreateWrapper(handler).Run();
		}

		public static IAsyncResult RaiseAsync<TArg>(Action<TArg> handler, TArg argument)
		{
			return EventWrapper.CreateWrapper(handler, argument).Run();
		}

		public static IAsyncResult RaiseAsync<TArg1, TArg2>(Action<TArg1, TArg2> handler, TArg1 arg1, TArg2 arg2)
		{
			return EventWrapper.CreateWrapper(handler, arg1, arg2).Run();
		}

		#region Nested types

		private static class EventWrapper
		{
			public static ActionEventWrapper CreateWrapper(Action handler) => new ActionEventWrapper(handler);

			public static ActionEventWrapper<TArg> CreateWrapper<TArg>(Action<TArg> handler, TArg argument) => new ActionEventWrapper<TArg>(handler, argument);

			public static ActionEventWrapper<TArg1, TArg2> CreateWrapper<TArg1, TArg2>(Action<TArg1, TArg2> handler, TArg1 arg1, TArg2 arg2)
			{
				return new ActionEventWrapper<TArg1, TArg2>(handler, arg1, arg2);
			}
		}

		private abstract class EventWrapperBase
		{
			private Action _eventCaller;

			protected internal EventWrapperBase()
			{
				_eventCaller = new Action(BeginCall);
			}

			public IAsyncResult Run()
			{
				return _eventCaller.BeginInvoke(new AsyncCallback(EndCall), null);
			}

			protected abstract void BeginCall();

			private void EndCall(IAsyncResult ar)
			{
				_eventCaller.EndInvoke(ar);
				_eventCaller = null;

				ar.AsyncWaitHandle.Close();

				Release();
			}

			protected abstract void Release();
		}

		private class ActionEventWrapper : EventWrapperBase
		{
			private Action _handler;

			internal ActionEventWrapper(Action handler)
			{
				_handler = handler;
			}

			protected override void BeginCall()
			{
				_handler?.Invoke();
			}

			protected override void Release()
			{
				_handler = null;
			}
		}

		private class ActionEventWrapper<TArgument> : EventWrapperBase
		{
			private Action<TArgument> _handler;
			private TArgument _argument;

			internal ActionEventWrapper(Action<TArgument> handler, TArgument argument) : base()
			{
				_handler = handler;
				_argument = argument;
			}

			protected override void BeginCall()
			{
				_handler?.Invoke(_argument);
			}

			protected override void Release()
			{
				_handler = null;
				_argument = default(TArgument);
			}
		}

		private class ActionEventWrapper<TArgument1, TArgument2> : EventWrapperBase
		{
			private Action<TArgument1, TArgument2> _handler;
			private TArgument1 _argument1;
			private TArgument2 _argument2;

			public ActionEventWrapper(Action<TArgument1, TArgument2> handler, TArgument1 argument1, TArgument2 argument2)
			{
				_handler = handler;
				_argument1 = argument1;
				_argument2 = argument2;
			}

			protected override void BeginCall()
			{
				_handler?.Invoke(_argument1, _argument2);
			}

			protected override void Release()
			{
				_handler = null;
				_argument1 = default(TArgument1);
				_argument2 = default(TArgument2);
			}
		}
		#endregion
	}
}