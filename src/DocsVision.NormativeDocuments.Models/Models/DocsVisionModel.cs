﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

using DocsVision.Platform;
using DocsVision.Platform.ObjectManager;

namespace DocsVision.NormativeDocuments.Models
{
	using Common;
	using Data;

	public class DocsVisionModel
	{
		#region Fields

		private readonly AssemblyResolver _resolver;
		private UserSession _currentSession;

		public UserSession Session => _currentSession;
		#endregion

		#region Events

		public event Action ConnectionEstablished;

		public event Action<Exception> ConnectionFailed;
		#endregion

		#region Constructors

		public DocsVisionModel()
		{
			_resolver = new AssemblyResolver();
		}
		#endregion

		#region Public class methods

		public async Task StartConnectAsync(DocsVisionConnectionParameters connectionParameters)
		{
			try
			{
				await Task.Delay(1000);
				
				_currentSession = CreateSession(connectionParameters);

				EventManager.RaiseAsync(ConnectionEstablished);
			}
			catch (Exception exc)
			{
				EventManager.RaiseAsync(ConnectionFailed, exc);
			}
		}
		#endregion

		#region Private class methods

		private UserSession CreateSession(DocsVisionConnectionParameters connectionParameters)
		{
			var sessionManager = ComFactory.CreateSessionManager();

			string serviceUrl = connectionParameters.ServiceUrl;
			string databaseName = connectionParameters.DatabaseName;
			string userName = connectionParameters.UserName;
			string password = connectionParameters.Password;

			sessionManager.Connect(serviceUrl, databaseName, userName, password);

			var session = sessionManager.CreateSession();
			return session;
		}
		#endregion
	}
}