﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

using Autofac.Integration.Presentation;

namespace DocsVision.NormativeDocuments.Views.Main
{
	using ViewModels.Main;

	public partial class RootView : Page, IView<RootViewModel>
	{
		public RootViewModel ViewModel => DataContext as RootViewModel;

		public RootView() : base()
		{
			InitializeComponent();
		}
	}
}