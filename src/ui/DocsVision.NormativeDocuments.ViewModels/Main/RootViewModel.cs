﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

using Autofac.Integration.Presentation;

namespace DocsVision.NormativeDocuments.ViewModels.Main
{
	using Data;
	using Models;

	public class RootViewModel : ViewModelBase
	{
		#region Fields and properties

		private readonly INavigationService _navigationService;
		private readonly DocsVisionModel _docsVisionModel;
		
		public ObservableProperty<string> ServiceUrl { get; } = string.Empty;

		public ObservableProperty<string> DatabaseName { get; } = string.Empty;

		public ObservableProperty<string> UserName { get; } = string.Empty;

		public ObservableProperty<string> Password { get; } = string.Empty;

		public ICommand StartConnectCommand { get; private set; }
		#endregion

		#region Constructors

		public RootViewModel(INavigationService navigationService, DocsVisionModel docsVisionModel) : base()
		{
			_navigationService = navigationService;
			_docsVisionModel = docsVisionModel;

			StartConnectCommand = new DelegateCommand(ConnectToDocsVision, CanConnectToDocsVision);
		}
		#endregion

		#region ViewAware methods overriding

		protected override void OnActivated()
		{
			base.OnActivated();

			_docsVisionModel.ConnectionEstablished += DocsVisionModelConnectionEstablished;
			_docsVisionModel.ConnectionFailed += DocsVisionModelConnectionFailed;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();

			_docsVisionModel.ConnectionFailed -= DocsVisionModelConnectionFailed;
			_docsVisionModel.ConnectionEstablished -= DocsVisionModelConnectionEstablished;
		}
		#endregion

		#region Private class methods

		private bool CanConnectToDocsVision()
		{
			return
				!string.IsNullOrWhiteSpace(ServiceUrl.Value) &&
				!string.IsNullOrWhiteSpace(DatabaseName.Value) &&
				!string.IsNullOrWhiteSpace(UserName.Value) &&
				!string.IsNullOrWhiteSpace(Password.Value);
		}

		private async void ConnectToDocsVision()
		{
			Mouse.OverrideCursor = Cursors.Wait;

			var parameters = new DocsVisionConnectionParameters(ServiceUrl.Value, DatabaseName.Value, UserName.Value, Password.Value);

			await _docsVisionModel.StartConnectAsync(parameters);
		}

		private void DocsVisionModelConnectionEstablished()
		{
			ExecuteOnUIThread(ResetCursor);

			MessageBox.Show("Connection established", "Success", MessageBoxButton.OK);

			// example for navigating to target view model after connection succeeded:
			/*
			ExecuteOnUIThread(() =>
			{
				_navigationService.NavigateToViewModel<SomeOtherViewModel>();
			});
			*/
		}

		private void DocsVisionModelConnectionFailed(Exception exc)
		{
			ExecuteOnUIThread(ResetCursor);

			MessageBox.Show(exc.StackTrace, exc.Message, MessageBoxButton.OK);
		}

		private static void ResetCursor()
		{
			Mouse.OverrideCursor = Cursors.Arrow;
		}
		#endregion
	}
}