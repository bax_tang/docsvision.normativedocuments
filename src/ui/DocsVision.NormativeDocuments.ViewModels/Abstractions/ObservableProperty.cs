﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DocsVision.NormativeDocuments.ViewModels
{
	public class ObservableProperty<TValue> : INotifyPropertyChanged
	{
		private static EqualityComparer<TValue> _comparer;
		private TValue _encapsulatedValue;

		public event PropertyChangedEventHandler PropertyChanged;

		public TValue Value
		{
			get => _encapsulatedValue;
			set
			{
				if (_comparer.Equals(_encapsulatedValue, value)) return;

				_encapsulatedValue = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
			}
		}

		static ObservableProperty()
		{
			_comparer = EqualityComparer<TValue>.Default;
		}

		public ObservableProperty() : this(default(TValue)) { }

		public ObservableProperty(TValue defaultValue)
		{
			_encapsulatedValue = defaultValue;
		}

		public static implicit operator ObservableProperty<TValue>(TValue value) => new ObservableProperty<TValue>(value);
	}
}