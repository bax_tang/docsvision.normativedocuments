﻿using System;

namespace DocsVision.NormativeDocuments.Data
{
	public class DocsVisionConnectionParameters
	{
		public string ServiceUrl { get; private set; }

		public string DatabaseName { get; private set; }

		public string UserName { get; private set; }

		public string Password { get; private set; }

		public DocsVisionConnectionParameters(string serviceUrl, string databaseName, string userName, string password)
		{
			ServiceUrl = serviceUrl;
			DatabaseName = databaseName;
			UserName = userName;
			Password = password;
		}
	}
}